part of coUwiki;

abstract class Location extends GameObject {
	/// Whether the street is accessible from inside the game
	bool inGame;

	Location(GameObjectType type, String id, String label, String category, String iconUrl, this.inGame)
		: super(type, id, label, category, iconUrl);
}
