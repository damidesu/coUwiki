part of coUwiki;

/**
 * Manages the downloading, parsing, and storing of JSON data from the server.
 */
class Data {
	/// Allow waiting for all data to download
	Completer<Data> load = new Completer();

	/// Stored data after downloading, categorized by type
	Map<GameObjectType, List<GameObject>> dataset = Map();

	/// Stored world map edges (for GPS)
	List<Map<String, dynamic>> worldEdges = List();

	/// Start caching and downloading data
	Data() {
		cache = new Cache();

		_loadAll().then((_) => load.complete(this));
	}

	/// Download and parse all data
	Future _loadAll() async {
		try {
			await _loadAchievements();
			await _loadEntities();
			await _loadItems();
			await _loadStreets();
			await _loadSkills();
			await _loadWorldEdges();
			cache.updateDate();
		} catch(e) {
			UI.showLoadError();
			throw e;
		}
	}

	/// Download and parse achivement data from the server's /listAchievements endpoint
	Future _loadAchievements() async {
		dataset[GameObjectType.Achievement] = new List();
		String json;

		if (cache.dataCurrent("achievements")) {
			json = cache.getData("achievements");
		} else {
			json = await HttpRequest.getString("${ServerUrl.SERVER}/listAchievements?token=$RS_TOKEN");
			cache.setData("achievements", json);
		}

		dataset[GameObjectType.Achievement].addAll(jsonDecode(json).values
			.cast<Map<String, dynamic>>()
			.map((Map<String, dynamic> map) => Achievement.fromJson(map))
			.toList()
			.cast<Achievement>());
	}

	/// Download and parse entity data from the server's /entities/list endpoint
	Future _loadEntities() async {
		dataset[GameObjectType.Entity] = new List();
		String json;

		if (cache.dataCurrent("entities")) {
			json = cache.getData("entities");
		} else {
			json = await HttpRequest.getString("${ServerUrl.SERVER}/entities/list?token=$RS_TOKEN");
			cache.setData("entities", json);
		}

		dataset[GameObjectType.Entity].addAll(jsonDecode(json)
			.cast<Map<String, dynamic>>()
			.where((Map<String, dynamic> value) => value != null)
			.map((Map<String, dynamic> map) => Entity.fromJson(map))
			.toList()
			.cast<Entity>());
	}

	/// Download and parse item data from the server's /getItems endpoint
	Future _loadItems() async {
		dataset[GameObjectType.Item] = new List();
		String json;

		if (cache.dataCurrent("items")) {
			json = cache.getData("items");
		} else {
			json = await HttpRequest.getString("${ServerUrl.SERVER}/getItems");
			cache.setData("items", json);
		}

		dataset[GameObjectType.Item].addAll(jsonDecode(json)
			.cast<Map<String, dynamic>>()
			.map((Map<String, dynamic> map) => Item.fromJson(map))
			.toList()
			.cast<Item>());
	}

	/// Download and parse street and hub data from the server's /getMapData endpoint
	Future _loadStreets() async {
		dataset[GameObjectType.Street] = new List();
		dataset[GameObjectType.Hub] = new List();

		String json;

		if (cache.dataCurrent("mapdata")) {
			json = cache.getData("mapdata");
		} else {
			json = await HttpRequest.getString("${ServerUrl.SERVER}/getMapData?token=$RS_TOKEN");
			cache.setData("mapdata", json);
		}

		jsonDecode(json)
			..["streets"].forEach((String label, dynamic data) {
				Street street = Street.fromJson(label, data as Map<String, dynamic>);

				if (street.inGame) {
					dataset[GameObjectType.Street].add(street);
				}
			})
			..["hubs"].forEach((String id, dynamic data) {
				Hub hub = Hub.fromJson(id, data as Map<String, dynamic>);

				if (hub.inGame) {
					dataset[GameObjectType.Hub].add(hub);
				}
			});
	}

	/// Download and parse skill data from the server's /skills/list endpoint
	Future _loadSkills() async {
		dataset[GameObjectType.Skill] = new List();
		String json;

		if (cache.dataCurrent("skills")) {
			json = cache.getData("skills");
		} else {
			json = await HttpRequest.getString("${ServerUrl.SERVER}/skills/list?token=$RS_TOKEN");
			cache.setData("skills", json);
		}

		dataset[GameObjectType.Skill].addAll(jsonDecode(json)
			.cast<Map<String, dynamic>>()
			.map((Map<String, dynamic> map) => Skill.fromJson(map))
			.toList()
			.cast<Skill>());
	}

	/// Download and parse the world graph from the json folder
	Future _loadWorldEdges() async {
		String json = await HttpRequest.getString("json/worldGraph.json");
		List<Map<String, dynamic>> edges = jsonDecode(json)["edges"]
			.cast<Map<String, dynamic>>().toList();
		worldEdges = edges;
	}
}
