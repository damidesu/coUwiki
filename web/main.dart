library coUwiki;

import "dart:async";
import "dart:convert";
import "dart:html";
import "dart:math";
import "dart:js";

import "package:couWiki/API_KEYS.dart";

part "package:couWiki/common/cache.dart";
part "package:couWiki/common/data.dart";
part "package:couWiki/common/globals.dart";
part "package:couWiki/common/objectpath.dart";
part "package:couWiki/common/util.dart";

part "package:couWiki/objects/achievement.dart";
part "package:couWiki/objects/entity.dart";
part "package:couWiki/objects/gameobject.dart";
part "package:couWiki/objects/hub.dart";
part "package:couWiki/objects/item.dart";
part "package:couWiki/objects/location.dart";
part "package:couWiki/objects/skill.dart";
part "package:couWiki/objects/street.dart";

part "package:couWiki/ui/listpage.dart";
part "package:couWiki/ui/locations_page.dart";
part "package:couWiki/ui/page.dart";
part "package:couWiki/ui/scroll.dart";
part "package:couWiki/ui/search.dart";
part "package:couWiki/ui/streetimage.dart";
part "package:couWiki/ui/ui.dart";

Future main() async {
	data = await new Data().load.future;
	ui = new UI();
}
